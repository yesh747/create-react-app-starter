// COLORS
export const TEAL = '#00B9BE';
export const DARK_TEAL = '#016B6E';
export const GREEN = '#67CE68';
export const BLACK = '#182F30';
export const BLACK_BG = 'rgba(24, 37, 48, 0.75)';
export const GRAY = '#96A0A0';
export const LIGHT_GRAY = '#EFEFEF';
export const MEDIUM_GRAY = '#D8D8D8';
export const DARK_GRAY = '#46595A';
export const ORANGE = '#E8911A';
export const RED = '#EF4D3E';
