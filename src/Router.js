import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import NavBar from './components/NavigationComponents/NavBar';
import Home from './components/HomeComponents/Home';
import Login from './components/LoginComponents/Login';

const Router = () => {
  return (
    <BrowserRouter>
      <div>
        <NavBar />
        <Switch>
          <Route path='/login' component={Login} />
          <Route path='/' component={Home} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default Router;
