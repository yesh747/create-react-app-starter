import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const NavBar = () => {
  const {
    navStyle, brandTextStyle, menuStyle, navItems
  } = styles;

  return (
    <nav className='navbar navbar-expand-lg navbar-light bg-light' style={navStyle}>
      <Link to='/' style={brandTextStyle}>Hello World</Link>
      <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
        <span className='navbar-toggler-icon' style={menuStyle} />
      </button>
      <div className='collapse navbar-collapse justify-content-end' id='navbarSupportedContent'>
        <ul className='navbar-nav' style={navItems}>
          <li className='nav-item'>
            <NavLink to='/' exact className='nav-item nav-link'>Home</NavLink>
          </li>
          <li className='nav-item'>
            <NavLink to='/login' exact className='nav-item nav-link'>Login</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

const styles = {
  navStyle: {
    padding: '10px',
  },
  brandTextStyle: {
    color: '#000',
    fontSize: '36px',
    fontWeight: '200',
    textDecoration: 'none'
  },
  menuStyle: {
    color: '#000',
  },
  navItems: {
    fontSize: '18px',
  }
};

export default NavBar;
